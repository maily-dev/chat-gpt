// pages/api/generate.ts
import { NextApiRequest, NextApiResponse } from "next";
import { Configuration, OpenAIApi } from "openai";

const configuration = new Configuration({
  apiKey: process.env.OPENAI_API_KEY,
});
const openai = new OpenAIApi(configuration);

export default async function handler(
  req: NextApiRequest,
  res: NextApiResponse
) {
  if (!configuration.apiKey) {
    res.status(500).json({
      error: {
        message:
          "OpenAI API key not configured, please follow instructions in README.md",
      },
    });
    return;
  }

  const message = req.body.message;

  try {
    const completion = await openai.createChatCompletion({
      model: "gpt-3.5-turbo",
      messages: [
        {
          role: "system", content: `
        You are an expert at writing specification documents for software projects.
        Your name is SparkPlan.
        Step by step:
        1. When user say Hi.  You will introduction about your sefl then asking What’s your name? Nice to meet you (name of user). You could ask step by step. If user donot answer, you will ask again.
        2. Just to be clear, what’s the Industry for <project code name>? Answer:….
        3. Asking user describe detail project. Example: you will ask about business model,...
        4. Let’s get started on your Specifications Document.
        
        In order to generate a fully-fledged expert specifications document for my project, list all questions you need to ask me and for each question give me a multiple choice to answer from with an "other" option where I can answer separately. The question is html, user can click to answer`},
        { role: "user", content: message },
      ],
      temperature: 0,
    });

    res.status(200).json({ result: completion.data.choices[0].message });
  } catch (error: any) {
    // Consider adjusting the error handling logic for your use case
    if (error.response) {
      console.error(error.response.status, error.response.data);
      res.status(error.response.status).json(error.response.data);
    } else {
      console.error(`Error with OpenAI API request: ${error.message}`);
      res.status(500).json({
        error: {
          message: "An error occurred during your request.",
        },
      });
    }
  }
}
